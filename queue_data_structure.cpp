#include <iostream>
using namespace std;

class Element{
public:
    int data;
    Element* next;
    Element* previous;
    Element(int d){
        data = d;
        next = NULL;
        previous = NULL;
    }
};

class Queue{
private:
    Element* head;
    Element* tail;
    int maxSize;
    int currentSize;
public:
    void insert (int d);
    void show();
    int remove ();
    Queue (int max){
        maxSize = max;
        head = NULL;
        currentSize = 0;
    }
};

void Queue::insert(int d) {
    Element* newElement = new Element (d);
    if (head == NULL){
        head = newElement;
        tail = newElement;
    }
    else{
        tail->next = newElement;
        newElement->previous = tail;
        tail = newElement;
    }
}

void Queue::show() {
    Element* temp = head;
    //while (temp){
        cout<<temp->data;
    //    temp = temp->next;
    //}
}

int Queue::remove() {
    if (head == NULL)
        return -1;
    Element* temp = head;
    head = temp->next;
    delete(temp);
    return 0;
}

int main() {
    Queue* list = new Queue(4);
    std::cout << "Hello, World!" << std::endl;
    list->insert(5);
    list->insert(6);
    list->insert(7);
    list->insert(8);
    list->insert(9);
    //list->remove(1);
    list->show();
    return 0;
}